# -*- coding: utf-8 -*-
"""
@author: Joel Smith
"""
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import sklearn.metrics as skmetrics
      
def unpickle(file):
    import pickle
    with open(file, 'rb') as fo:
        dict = pickle.load(fo, encoding='bytes')
    return dict

def labels_to_onehot(labels, n_values):
    n_labels = len(labels)
    onehot_labels = np.empty((n_labels, n_values))
    for i in range(n_labels):
        onehot_labels[i] = [1 if j==labels[i] else 0 for j in range(n_values)]
    return onehot_labels

def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)

def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)

def conv2d(x, W):
    return tf.nn.conv2d(x, W, strides=[1,1,1,1], padding='SAME')

def max_pool_2x2(x):
    return tf.nn.max_pool(x, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME')

class CustomData:
    def __init__(self, data, labels):
        self.data = data
        self.labels = labels
        self.current_idx = 0
        self.n_samples = len(data)
        
    def next_batch(self, batch_size):
        if self.current_idx + batch_size > self.n_samples - 1:
            self.current_idx = 0
        batch_data = self.data[self.current_idx:self.current_idx+batch_size]
        batch_labels = self.labels[self.current_idx:self.current_idx+batch_size]
        self.current_idx += batch_size
        return batch_data, batch_labels
    
def evaluate_accuracy(tf_accuracy, test_x, test_y):
    segment_length = 1000
    n_segments = len(test_x) // segment_length
    acc = 0
    for i in range(n_segments):
        x_i = test_x[i*segment_length : (i+1)*segment_length]
        y_i = test_y[i*segment_length : (i+1)*segment_length]
        acc += accuracy.eval(feed_dict={x:x_i, y_:y_i,
                                        keep_prob_fc:0.5})
    return acc / n_segments

if __name__ == '__main__':
    # import data
    print('Running...')
    # get the training data
    filename = 'data_batch_1'
    file = unpickle(filename)
    data = file[b'data']
    labels = file[b'labels']
    onehot_labels = labels_to_onehot(labels, 10)
    for i in range(2, 6):
        filename = 'data_batch_' + str(i)
        file = unpickle(filename)
        data_i = file[b'data']
        labels = file[b'labels']
        onehot_labels_i = labels_to_onehot(labels, 10)
        data = np.append(data, data_i, axis=0)
        onehot_labels = np.append(onehot_labels, onehot_labels_i, axis=0)
    onehot_labels = np.append(onehot_labels, onehot_labels, axis=0)
    data = data.reshape((-1, 32, 32, 3), order='F')
    data = np.rot90(data, k=-1, axes=(1,2))
    data = np.append(data, np.flip(data, axis=2), axis=0)
    custom_data = CustomData(data, onehot_labels)
    # get the testing data
    filename = 'test_batch'
    file = unpickle(filename)
    test_data = file[b'data']
    test_data = test_data.reshape((-1, 32, 32, 3), order='F')
    test_data = np.rot90(test_data, k=-1, axes=(1,2))
    test_labels = file[b'labels']
    test_onehot_labels = labels_to_onehot(test_labels, 10)
    print('Learning...')

    
    ## set up graph
    # the input data
    x = tf.placeholder(tf.float32, [None, 32, 32, 3])
    x_image = tf.reshape(x, [-1, 32, 32, 3])
    # first convolution: 3x3 patch, 3 input channels, 64 out channels
    W_conv11 = weight_variable([5, 5, 3, 64])
    b_conv11 = bias_variable([64])
    h_conv11 = conv2d(x_image, W_conv11) + b_conv11
    h_norm11 = tf.contrib.layers.batch_norm(h_conv11)
    h_relu11 = tf.nn.relu(h_norm11)
    # second convolution
    W_conv12 = weight_variable([5, 5, 64, 64])
    b_conv12 = bias_variable([64])
    h_conv12 = conv2d(h_relu11, W_conv12) + b_conv12
    h_norm12 = tf.contrib.layers.batch_norm(h_conv12)
    h_relu12 = tf.nn.relu(h_norm12)
    # third convolution
    W_conv13 = weight_variable([5, 5, 64, 64])
    b_conv13 = bias_variable([64])
    h_conv13 = conv2d(h_relu12, W_conv13) + b_conv13
    h_norm13 = tf.contrib.layers.batch_norm(h_conv13)
    h_relu13 = tf.nn.relu(h_norm13)
    # fourth convolution
    W_conv14 = weight_variable([5, 5, 64, 64])
    b_conv14 = bias_variable([64])
    h_conv14 = conv2d(h_relu13, W_conv14) + b_conv14
    h_norm14 = tf.contrib.layers.batch_norm(h_conv14)
    h_relu14 = tf.nn.relu(h_norm14)
    h_pool14 = max_pool_2x2(h_relu14)
    # fifth convolution
    W_conv21 = weight_variable([3, 3, 64, 128])
    b_conv21 = bias_variable([128])
    h_conv21 = conv2d(h_pool14, W_conv21) + b_conv21
    h_norm21 = tf.contrib.layers.batch_norm(h_conv21)
    h_relu21 = tf.nn.relu(h_norm21)
    # sixth convolution
    W_conv22 = weight_variable([3, 3, 128, 128])
    b_conv22 = bias_variable([128])
    h_conv22 = conv2d(h_relu21, W_conv22) + b_conv22
    h_norm22 = tf.contrib.layers.batch_norm(h_conv22)
    h_relu22 = tf.nn.relu(h_norm22)
    # seventh convolution
    W_conv23 = weight_variable([3, 3, 128, 128])
    b_conv23 = bias_variable([128])
    h_conv23 = conv2d(h_relu22, W_conv23) + b_conv23
    h_norm23 = tf.contrib.layers.batch_norm(h_conv23)
    h_relu23 = tf.nn.relu(h_norm23)
    # eighth convolution
    W_conv24 = weight_variable([3, 3, 128, 128])
    b_conv24 = bias_variable([128])
    h_conv24 = conv2d(h_relu23, W_conv24) + b_conv24
    h_norm24 = tf.contrib.layers.batch_norm(h_conv24)
    h_relu24 = tf.nn.relu(h_norm24)
    h_pool24 = max_pool_2x2(h_norm24)
    # ninth convolution
    W_conv31 = weight_variable([3, 3, 128, 256])
    b_conv31 = bias_variable([256])
    h_conv31 = conv2d(h_pool24, W_conv31) + b_conv31
    h_norm31 = tf.contrib.layers.batch_norm(h_conv31)
    h_relu31 = tf.nn.relu(h_norm31)
    # tenth convolution
    W_conv32 = weight_variable([3, 3, 256, 256])
    b_conv32 = bias_variable([256])
    h_conv32 = conv2d(h_relu31, W_conv32) + b_conv32
    h_norm32 = tf.contrib.layers.batch_norm(h_conv32)
    h_relu32 = tf.nn.relu(h_norm32)
    # eleventh convolution
    W_conv33 = weight_variable([3, 3, 256, 256])
    b_conv33 = bias_variable([256])
    h_conv33 = conv2d(h_relu32, W_conv33) + b_conv33
    h_norm33 = tf.contrib.layers.batch_norm(h_conv33)
    h_relu33 = tf.nn.relu(h_norm33)
    # twelfth convolution
    W_conv34 = weight_variable([3, 3, 256, 256])
    b_conv34 = bias_variable([256])
    h_conv34 = conv2d(h_relu33, W_conv34) + b_conv34
    h_norm34 = tf.contrib.layers.batch_norm(h_conv34)
    h_relu34 = tf.nn.relu(h_norm34)
    keep_prob_fc = tf.placeholder(tf.float32)
    h_drop34 = tf.nn.dropout(h_relu34, keep_prob_fc)
    # first fully-connected layer (256 neurons)
    # image has been max-pooled twice and is now 8x8x256ch
    W_fc1 = weight_variable([8*8*256, 256])
    b_fc1 = bias_variable([256])
    h_relu7_flat = tf.reshape(h_drop34, [-1, 8*8*256])
    h_fc1 = tf.nn.relu(tf.matmul(h_relu7_flat, W_fc1) + b_fc1)
    h_fc1_norm = tf.contrib.layers.batch_norm(h_fc1)
    h_fc1_drop = tf.nn.dropout(h_fc1_norm, keep_prob_fc)
    # second fully-connected layer (128 neurons)
    W_fc2 = weight_variable([256, 128])
    b_fc2 = bias_variable([128])
    h_fc2 = tf.nn.relu(tf.matmul(h_fc1_drop, W_fc2) + b_fc2)
    h_fc2_norm = tf.contrib.layers.batch_norm(h_fc2)
    h_fc2_drop = tf.nn.dropout(h_fc2_norm, keep_prob_fc)
    # third fully-connected layer (10 neurons)
    W_fc3 = weight_variable([128, 10])
    b_fc3 = bias_variable([10])
    y = tf.matmul(h_fc2_drop, W_fc3) + b_fc3
    
    ## set up a cross-entropy minimization optimizer
    y_ = tf.placeholder(tf.float32, [None, 10])
    cross_entropy = tf.reduce_mean(
        tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y))
    # learning rate schedule
    starting_learning_rate = 2e-3
    global_step = tf.Variable(0, trainable=False)
    decay_steps = 3000
    decay_rate = 0.5
    learning_rate = tf.train.exponential_decay(
            starting_learning_rate, global_step, decay_steps, decay_rate)
    train_step = tf.train.AdamOptimizer(learning_rate).minimize(
            cross_entropy, global_step=global_step)
    correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    
    ## run the training loop
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        for i in range(10000):
            batch, labels = custom_data.next_batch(64)
            train_step.run(feed_dict={
                    x:batch,
                    y_:labels,
                    keep_prob_fc:0.5})
            if i % 100 == 0:
#                z = batch[0].reshape((-1, 32, 32, 3))
#                plt.imshow(z[0])
                train_accuracy = accuracy.eval(feed_dict={
                    x:batch,
                    y_:labels,
                    keep_prob_fc:1})
                print('step %d, training accuracy %g' % (i, train_accuracy))
            if i % 1000 == 0:
                print('step %d, test accuracy %g' % 
                      (i, evaluate_accuracy(
                              accuracy,
                              test_data,
                              test_onehot_labels)))
                
                
        
#        print('test accuracy %g' % evaluate_accuracy(
#                accuracy,
#                test_data,
#                test_onehot_labels))
        output = sess.run(y, feed_dict={
                    x:test_data,
                    keep_prob_fc:1})
        predictions = np.argmax(output, axis=1)
        conf_mat = skmetrics.confusion_matrix(test_labels, predictions)
        fig, ax = plt.subplots()
        ax.matshow(conf_mat)
        for (i, j), z in np.ndenumerate(conf_mat):
            ax.text(j, i, '{:d}'.format(z), ha='center', va='center')
        plt.show()
